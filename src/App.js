import React from 'react';
import { BrowserRouter , Routes, Route} from 'react-router-dom';
import Register from './components/Register/index.js';
import Login from './components/Login/index.js';
import './App.css';
import Home from './components/Home/index.js';
import Contact from './components/Contact/index.js';
import Header from './components/Header/index.js';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
         <Header />
         <Routes>
               <Route path='/register' element={<Register />}/>
               <Route path='/login' element={<Login />}/>
               <Route path='/' element={<Home />}/>
               <Route path='/contact' element={<Contact />}/>
         </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
