import React from "react";
import {FaBuilding, FaAddressCard, FaPhone} from 'react-icons/fa'
import {AiOutlineMail} from  'react-icons/ai'
import Header from "../Header";

const Contact =()=> {
    return (
       <div className="h-screen flex justify-center items-center">
        <div className="rounded-lg bg-gray-200 w-3/12 h-60 shadow-xl p-4 m-4 flex flex-col justify-center items-start font-sans md:font-serif">
            <p className="text-lg items-center flex"><FaBuilding className="text-xl self-center mr-2"/><span className="text-2xl font-semibold mr-2">Company : </span> Geeksynergy Technologies Pvt Ltd</p>
            <p className="text-lg items-center flex"><FaAddressCard className="text-xl self-center mr-2"/><span className="text-2xl font-semibold mr-2">Address :</span> Sanjayanagar, Bengaluru-56</p>
            <p className="text-lg items-center flex"><FaPhone className="text-xl self-center mr-2"/><span className="text-2xl font-semibold mr-2">Phone :</span> 9123456789</p>
            <p className="text-lg items-center flex"><AiOutlineMail className="text-xl self-center mr-2"/><span className="text-2xl font-semibold mr-2">Email :</span> geeksynergy@gmail.com</p>
        </div>
      </div>
    )
}

export default Contact;