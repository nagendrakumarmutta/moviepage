import React from "react";
import{VscTriangleDown, VscTriangleUp} from "react-icons/vsc"

const MovieCard = (props) => {
    const {eachItem} = props;
    const {voted, poster, title, genre, director, stars,language, pageViews} = eachItem;
    console.log(eachItem);

    return (
        <div className="rounded-lg bg-slate-100 w-3/12 h-60 shadow-md p-4 m-4 hover:scale-110">
            <div className="flex">
            <div className="flex flex-col w-11 mr-5">
                <>
                <VscTriangleUp className="text-5xl"/>
                <p className="text-center text-2xl m-0 p-0">{voted.length}</p>
                <VscTriangleDown className="text-5xl"/>
                </>
                <p className="font-sans md:font-serif font-semibold text-gray-600">Votes</p>
            </div>

            <img 
             src={poster}
             alt=""
             className="h-36 w-20 rounded-md mr-5"/>

             <div className="text-left">
                <h1 className="text-2xl font-sans md:font-serif font-medium">{title}</h1>
                <p className="text-sm font-sans md:font-serif font-medium mt-1 text-gray-700"><span className="text-gray-700 font-semibold">Genre:</span>{genre}</p>
                <p className="text-sm font-sans md:font-serif font-medium text-gray-700"><span className="text-gray-700 font-semibold">Director:</span>{director[0]}</p>
                <p className="text-sm font-sans md:font-serif font-medium text-gray-700"><span className="text-gray-700 font-semibold">Starring:</span> {stars[0].slice(0,25)} </p>
                <p className="text-sm font-sans md:font-serif font-medium text-gray-700"><span className="text-gray-700 font-semibold">Language:</span>{language}</p>
                <p className="text-md text-green-600 font-sans md:font-serif font-semibold mt-1">{pageViews} Views | Voted by {voted.length} people</p>
             </div>
        </div>
            <button type="button" className="font-sans md:font-serif font-semibold bg-sky-500 text-white mt-2 mb-0 w-full h-8 rounded-md">Watch Trailer</button>
        </div>    )
}

export default MovieCard;