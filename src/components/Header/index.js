import React, { useState }  from "react";
import {BiMoviePlay} from  'react-icons/bi'
import { Link } from "react-router-dom";

const Header = ()=> {
    const [value, setValue] = useState(true);

    return (
        <div className="bg-violet-500 h-16 flex justify-between items-center pl-5 pr-5 fixed w-full">
              <Link to="/"> 
            <div className="flex text-4xl text-white">
            <BiMoviePlay />  <h1 className="text-white text-3xl font-sans md:font-serif font-semibold">Movies</h1>
            </div>
            </Link>

            <h1 className="text-white text-xl font-sans md:font-serif font-semibold">About</h1>

            
             <Link to="/contact"><h1 className="text-white text-xl font-sans md:font-serif font-semibold cursor-pointer">Contact</h1></Link> 
            
            {value ? <Link to="/login"> 
            <button type="button" onClick={()=>{setValue(!value)}} className="bg-gray-600 w-24 h-11 text-lg rounded-md text-white md:font-serif font-semibold cursor-pointer">Login</button>
            </Link> : 
            <Link to="/register"> 
            <button type="button" onClick={()=>{setValue(!value)}} className="bg-gray-600 w-24 h-11 text-lg rounded-md text-white md:font-serif font-semibold cursor-pointer">Logout</button>
            </Link>}

             
        </div>
    )
}

export default Header;