import React, { useEffect, useState } from "react";
import Footer from "../Footer";
import Header from "../Header";
import MovieCard from "../MovieCard";

const Home =()=> {
    const [jsonData, setJsonData] = useState("");
    console.log(jsonData);
    
    useEffect(()=>{
        async function fetchApi(){
           const bodyData = {
            category: "movies",
            language: "kannada", 
            genre: "all",
            sort: "voting"
           }
          const options = {
            method : "POST",
            headers: {
                "Content-Type":"application/json",
            },
            body : JSON.stringify(bodyData),
          }

          const response =  await fetch("https://hoblist.com/api/movieList", options);
          const data = await response.json();
          setJsonData(data);
        }
          
        fetchApi();
    }, [])

    return(
        <>
        <Header />
        <div className="flex flex-wrap justify-center pt-16 h-fit overflow-hidden">
            {jsonData?.result?.map(eachItem => (
                <MovieCard eachItem = {eachItem} key={eachItem._id}/>
            ))}
        </div>
        <Footer />
        </>
    )
}

export default Home;