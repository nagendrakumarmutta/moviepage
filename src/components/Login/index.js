import React, { useRef } from "react";
import { useNavigate } from "react-router-dom";
import "../../input.css"

const Login =()=>{
    const Email = useRef(null);
    const Password = useRef(null);
    const navigate = useNavigate()

    const handleLoginSubmit=(e)=> {
        e.preventDefault();

        const email = Email.current.value;
        const password = Password.current.value;

        const data = JSON.parse(localStorage.getItem("data"));
        let counter = 0;
        for(let item of data){
            if(item.email === email && item.password === password){
                counter++
            }}

        if(counter>0){
            navigate("/");
        }else{
            alert("Invalid User")
        }

        Email.current.value="";
        Password.current.value="";
    }

    return(
    <div className="bg-slate-200 min-h-screen flex justify-center items-center">
    <form onSubmit={handleLoginSubmit} className="bg-white w-3/12 h-2/5 rounded-lg shadow-black p-3 font-sans md:font-serif">

      <h1 className="text-3xl font-semibold">Login</h1>

      <div className="flex flex-col text-left p-3.5 mt-0 pt-0">
       <label htmlFor="emailInput" className="text-lg font-normal">
              Email
       </label>
       <input id="emailInput" ref={Email} className="h-8 focus:outline-none border border-solid border-gray-600 rounded" required/>
      </div>

      <div className="flex flex-col text-left p-3.5 mt-0 pt-0">
       <label htmlFor="pswdInput" className="text-lg font-normal">
              Password
       </label>
       <input id="pswdInput" ref={Password} type="password" className="h-8 focus:outline-none border border-solid border-gray-600 rounded" required/>
      </div>

      <button type="submit" className="bg-slate-500 w-20 h-9 rounded-md text-white">Login</button>

    </form>
    </div>
)
}

export default Login;