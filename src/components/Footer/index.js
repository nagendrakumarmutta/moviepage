import React from "react";
import {BsFacebook, BsTwitter} from "react-icons/bs";

const Footer = ()=> {
    return(
        <div className="bg-black h-24 flex items-center">
        <div className="flex justify-around w-full">
          <div className="flex justify-between w-24">
            <a
              href="https://twitter.com/makemytrip/"
              className="anchor-element"
              target="_blank"
            >
              <BsTwitter className="text-white text-3xl" />
            </a>
            <a
              href="https://www.facebook.com/makemytrip/"
              className="anchor-element"
              target="_blank"
            >
              <BsFacebook className="text-white text-3xl" />
            </a>
          </div>
          <div className="text-white">
            <p>© 2022 MOVIE PAGE PVT. LTD.</p>
            <p>Country India USA UAE</p>
          </div>
        </div>
      </div>

    )
}

export default Footer;