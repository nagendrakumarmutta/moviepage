import React, { useRef } from "react";
import { useNavigate } from "react-router-dom";
import "../../input.css"

const Register =()=>{
    const Name = useRef(null);
    const Email = useRef(null);
    const Password = useRef(null);
    const PhoneNumber = useRef(null);
    const Profession = useRef(null);
    let dataArray = [];

    const navigate = useNavigate()

    const handleSubmit =(e) => {
          e.preventDefault();
        
          let data = {
            name:Name.current.value,
            email:Email.current.value,
            password:Password.current.value,
            phoneNumber:PhoneNumber.current.value,
            profession:Profession.current.value
          }
          

          dataArray.push(data)
          
          dataArray = JSON.stringify(dataArray);
          
          localStorage.setItem('data', dataArray);
          dataArray = JSON.parse(dataArray);

          Name.current.value="";
          Email.current.value="";
          Password.current.value="";
          PhoneNumber.current.value="";
          Profession.current.value="";

          navigate("/login");
    }

    return (
    <div className="bg-slate-200 min-h-screen flex justify-center items-center">
      <form onSubmit={handleSubmit} className="bg-white w-3/12 h-2/5 rounded-lg shadow-black p-3 font-sans md:font-serif">

        <h1 className="text-3xl font-semibold">Register</h1>

        <div className="flex flex-col text-left p-3.5 mb-0">
         <label htmlFor="nameInput" className="text-lg font-normal">
                Name
         </label>
         <input id="nameInput" ref={Name} className="h-8 focus:outline-none border border-solid border-gray-600 rounded" required/>
        </div>

        <div className="flex flex-col text-left p-3.5 mt-0 pt-0">
         <label htmlFor="emailInput" className="text-lg font-normal">
                Email
         </label>
         <input id="emailInput" ref={Email} className="h-8 focus:outline-none border border-solid border-gray-600 rounded" required/>
        </div>

        <div className="flex flex-col text-left p-3.5 mt-0 pt-0">
         <label htmlFor="pswdInput" className="text-lg font-normal">
                Password
         </label>
         <input id="pswdInput" ref={Password} type="password" className="h-8 focus:outline-none border border-solid border-gray-600 rounded" required/>
        </div>

        <div className="flex flex-col text-left p-3.5 mt-0 pt-0">
         <label htmlFor="phInput" className="text-lg font-normal">
                Phone Number
         </label>
         <input id="phInput" ref={PhoneNumber} type="number" className="h-8 focus:outline-none border border-solid border-gray-600 rounded" required/>
        </div>

        <div className="flex flex-col text-left p-3.5 mt-0 pt-0">
         <label htmlFor="professionInput" className="text-lg font-normal">
                Profession
         </label>
        <select id="professionInput" ref={Profession} className="h-8 focus:outline-none border border-solid border-gray-600 rounded bg-transparent">
                  
                 <option defaultValue="Engineer">Engineer</option>
                 <option value="Doctor">Doctor</option>
                 <option value="Actor">Actor</option>
                 <option value="Lawyer">Lawyer</option>
                 <option value="Business">Business</option>
                 <option value="Teacher">Teacher</option>
                 <option value="Police">Police</option>
                 <option value="Others">Others</option>

        </select>
        </div>

        <button type="submit" className="bg-slate-500 w-20 h-9 rounded-md text-white">Submit</button>

      </form>
       
    </div>
    )
}


export default Register;
